package br.com.itau.UsuarioCEPZipKin.repository;

import br.com.itau.UsuarioCEPZipKin.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository <Usuario, Integer> {
}
