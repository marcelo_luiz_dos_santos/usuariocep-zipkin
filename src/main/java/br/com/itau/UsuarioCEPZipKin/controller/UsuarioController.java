package br.com.itau.UsuarioCEPZipKin.controller;

import br.com.itau.UsuarioCEPZipKin.model.Usuario;
import br.com.itau.UsuarioCEPZipKin.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuariocep")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario criarUsuario (@RequestBody @Valid Usuario usuario){
        Usuario usuarioObjeto = usuarioService.criarUsuario(usuario);
        return usuarioObjeto;
    }
}
