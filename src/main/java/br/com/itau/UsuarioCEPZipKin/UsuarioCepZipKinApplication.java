package br.com.itau.UsuarioCEPZipKin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class UsuarioCepZipKinApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsuarioCepZipKinApplication.class, args);
	}

}
