package br.com.itau.UsuarioCEPZipKin.service;

import br.com.itau.UsuarioCEPZipKin.Cliente.Cep;
import br.com.itau.UsuarioCEPZipKin.Cliente.CepClient;
import br.com.itau.UsuarioCEPZipKin.model.Usuario;
import br.com.itau.UsuarioCEPZipKin.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private CepClient cepClient;

    public Usuario criarUsuario(Usuario usuario){
        Cep cep = cepClient.getCep(usuario.getCep());
        usuario.setBairro(cep.getBairro());
        usuario.setLogradouro(cep.getLogradouro());

        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }
}
