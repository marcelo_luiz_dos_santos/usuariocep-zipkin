package br.com.itau.UsuarioCEPZipKin.Cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CEP", configuration = CepClientConfiguration.class)
public interface CepClient {
    @GetMapping("/CEP/{cep}")
    Cep getCep(@PathVariable String cep);
}
