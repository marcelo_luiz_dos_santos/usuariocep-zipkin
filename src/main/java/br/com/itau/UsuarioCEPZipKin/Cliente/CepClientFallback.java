package br.com.itau.UsuarioCEPZipKin.Cliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CepClientFallback implements CepClient {

    @Override
    public Cep getCep(String cep) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço CEP indisponível");
    }
}
